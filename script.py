from paraview import simple as pvsimple

pvsimple.LoadPlugin("openpmd_reader.py")
r = pvsimple.openPMDReader(FileName = "/Users/berk/Work/openPMD/openPMD-example-datasets/example-3d/hdf5/data.pmd")
r.UpdatePipelineInformation()
# print("Timesteps:", r.TimestepValues)
r.Arrays = ['rho', 'E']
r.Species = ['electrons']
# r.ParticleArrays = ['electrons_mass']
# print("Arrays:", r.Arrays)
r.UpdatePipeline()
# print('here')
# pvsimple.Show(r) # Why does this execute again?

# from paraview import servermanager as pvsm

# print(pvsm.OutputPort(r, 1))
# f = pvsimple.MergeBlocks()
# f.Input = pvsm.OutputPort(r, 1)

# pvsimple.SaveData("data.pvtu", proxy=f)