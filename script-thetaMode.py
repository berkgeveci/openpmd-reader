from paraview import simple as pvsimple

pvsimple.LoadPlugin("openpmd_reader.py")
r = pvsimple.openPMDReader(FileName = "/Users/berk/Work/openPMD/openPMD-example-datasets/example-thetaMode/hdf5/data.pmd")
r.UpdatePipelineInformation()
print("Timesteps:", r.TimestepValues)
# r.Arrays = ['rho', 'E_x']
# r.Species = ['electrons']
# r.ParticleArrays = ['electrons_mass']
# print("Arrays:", r.Arrays)
r.UpdatePipeline()

f = pvsimple.MergeBlocks(r)

pvsimple.SaveData("data.pvtu", proxy=f)